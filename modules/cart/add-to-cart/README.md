# Add a Product to Cart 
This has been tested with Coredna Release 2018.10 

## Requirements 
0. A webpack or gulp build system with Babel to convert ES6 JS code to ES5
0. A webpack or gulp build system to convert SCSS to CSS
0. The JS event binding relies on a form submit button with a class name `cart-add`. Please make sure your form submit button has this class in order to make use of JS code.

## Limitation
0. It works when a website does not use Variants 
0. It works when a website uses Variants in a way that each variant has options from only one option list

## Assumptions
0. It is assumed that we have `$product` object available on product page when using this snippet. Please replace this object with your own object variable in case you are using it elsewhere. 
 
## Files available in this snippet
* **`product.html`** It contains a very simple form that can be used to add a product to cart. This snippet should be used per product basis. 
* **`cart.js`** It contains ES6 module to handle Add to Cart functionality using AJAX. It has necessary functions to process form submit and response from Coredna.
* **`cart.scss`** Some basic SCSS styles which you may not need. You can ignore this file if not required. 

## How to use this snippet 

Old school way would be to copy paste the code and hope it does something. A better way is to understand the code snippet first and then use it in your project. 

#### product.html 

This is an HTML form that displays product name, input quantity field and product options dropdown if we are using variants. You can use this form anywhere you would like to enable Add to Cart functionality. We can add a product to cart by submitting this form.

#### cart.js
It is built as an ES6 JS Module. By default, we export the module from this file. You can use this module in your main application to initialise it.
```
import CartAdder from './cart'     
CartAdder.init()
``` 

If you are not using ES6 imports, you can simply uncomment this code `CartAdder.init()` in `cart.js` and include this file in your distribution bundle.

#### cart.scss
It has basic form styles if required. 