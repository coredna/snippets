/**
 * cart.js
 * Basic Add to Cart functionality using AJAX
 * It requires Babel transpiler to convert ES6
 */


/**
 * Enable this import if you don't have jQuery in global scope
 */
//import $ from 'jquery'

const CartAdder = (() => {

    let submit = $('.cart-add')

    const updateHeader = () => {
        // write your own code to update UI; for example items count in header
    }

    const handleCriticalError = (err) => {
        console.error(err)
        // Update UI to say something went wrong
    }

    const processErrors = (errors) => {
        alert(errors.join('<br>'))
        // Update UI to show errors
    }

    const processSuccess = (response) => {
        alert(response.success)
        // Update UI to show success message
    }

    const processAjaxResponse = (response) => {
        if(response.success) {
            processSuccess(response)
        }
        else if(response.errors) {
            processErrors(response.errors)
        }
        else {
            handleCriticalError('UNRECOGNISED RESPONSE')
        }
    }

    const initAjaxAdd = (data) => {
        $.ajax({
            url: '/index.php?action=prodcataloguecart&json=1',
            type: 'post',
            dataType: "json",
            data: data ,
            success: processAjaxResponse,
            error: handleCriticalError
        }).done(function (msg) {
            // do something useful
        }).always(function () {
            updateHeader()
            // do something more useful
        })
    }

    const getProductId = ($form) => (
        $form.find('input[name=product_id]').val()
    )

    const getProductQuantity = ($form) => (
        $form.find('input[name=quantity]').val() || 1
    )

    const addProductToCart = ($self) => {
        let form = $self.closest('form')
        initAjaxAdd(form.serialize())
    }

    const events = () => {
        submit.on('click', (event) => {
            event.preventDefault()
            addProductToCart($(event.currentTarget))
        })
        // your event bindings should be here
    }

    const init = () => {
        events()
    }

    return {
        init: init
    }

})();


/**
 * If desired you can uncomment following code to run the init function
 */
//CartAdder.init()

/**
 * You can export the module here. Comment the following line if you don't require export
 */
export default CartAdder